import pytest

from code import numberDivisibleBy3

class Test_class:

    def test_add_two_number(self):
        assert 16 == numberDivisibleBy3.add_two_number(6,10)

    def test_add_two_number_str_one(self):
        assert '30' == numberDivisibleBy3.add_two_number('20', 10) #one string input

    def test_add_two_number_both_str(self):
        assert '30' == numberDivisibleBy3.add_two_number('20', '10') #both string input

    def test_add_two_number_one_none(self):
        assert numberDivisibleBy3.add_two_number(None,10) is None

    def test_add_two_number_none(self):
        assert numberDivisibleBy3.add_two_number(None,None) is None

